package com.asme.androidclient.service;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.asme.androidclient.domain.ApplicationMode;
import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.UserInfoResponse;
import com.facebook.android.crypto.keychain.AndroidConceal;
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.CryptoConfig;
import com.facebook.crypto.Entity;
import com.facebook.crypto.keychain.KeyChain;

import java.nio.charset.Charset;
import java.util.Date;

import okhttp3.WebSocket;

/**
 * Created by vmy on 04.04.18.
 */

public class ContextService {

    public static final String API_SERVER_URL = "http://192.168.2.101:8080";
    public static final String WEBSOCKET_API_SERVER_URL = "http://192.168.2.101:8081";
    public static final String AUTH_SERVER_URL = "http://192.168.2.101:8084";

    private final static String TOKENS_STORAGE_NAME = "com.asme.security.TOKENS";
    private final static String ENCODED_ACCESS_TOKEN = "com.asme.security.TOKENS.ACCESS_TOKEN";
    private final static String ENCODED_REFRESH_TOKEN = "com.asme.security.TOKENS.REFRESH_TOKEN";
    private final static String ENCODED_ACCESS_TOKEN_EXPIRY_DATE = "com.asme.security.TOKENS.ACCESS_TOKEN.expiry_date";

    private final static String USER_STORAGE_NAME = "com.asme.security.USER";
    private final static String USER_ID = "com.asme.security.USER.user_id";
    private final static String USERNAME = "com.asme.security.USER.username";
    private final static String EMAIL = "com.asme.security.USER.email";

    private final static Entity accessTokenEntity = Entity.create(ENCODED_ACCESS_TOKEN);
    private final static Entity refreshTokenEntity = Entity.create(ENCODED_REFRESH_TOKEN);

    private static ApplicationMode applicationMode;

    private static Crypto crypto;

    private static UserInfoResponse userInfo;

    private static String refreshToken;

    private static String accessToken;

    private static Date accessTokenExpiryDate;

    private static WebSocket webSocket;

    private static Long driveRequestId;

    private static Long driveRouteId;

    static {
        initialize();
    }

    private static void initialize() {

    }

    public static void saveApplicationMode(ApplicationMode mode) {
        ContextService.applicationMode = mode;
    }

    public static void saveToken(Context context, JwtTokenResponse tokenResponse) {

        Crypto crypto = getOrCreateCrypto(context);

        try {
            ContextService.accessToken = tokenResponse.getAccessToken();
            ContextService.refreshToken = tokenResponse.getRefreshToken();
            ContextService.accessTokenExpiryDate = tokenResponse.getAccessTokenExpiryDate();
            ContextService.userInfo = tokenResponse.getUserInfo();

            byte[] cipherText = crypto.encrypt(tokenResponse.getAccessToken().getBytes(Charset.forName("UTF-8")), accessTokenEntity);
            String encodedAccessToken = Base64.encodeToString(cipherText, Base64.DEFAULT);;

            cipherText = crypto.encrypt(tokenResponse.getRefreshToken().getBytes(Charset.forName("UTF-8")), refreshTokenEntity);
            String encodedRefreshToken = Base64.encodeToString(cipherText, Base64.DEFAULT);

            SharedPreferences sharedPrefTokens = context.getSharedPreferences(TOKENS_STORAGE_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editorTokens = sharedPrefTokens.edit();
            editorTokens.putString(ENCODED_ACCESS_TOKEN, encodedAccessToken);
            editorTokens.putString(ENCODED_REFRESH_TOKEN, encodedRefreshToken);
            editorTokens.putLong(ENCODED_ACCESS_TOKEN_EXPIRY_DATE, ContextService.accessTokenExpiryDate.getTime());
            editorTokens.apply();

            SharedPreferences sharedPrefUser = context.getSharedPreferences(USER_STORAGE_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editorUser = sharedPrefUser.edit();
            editorUser.putLong(USER_ID, ContextService.userInfo.getUserId());
            editorUser.putString(USERNAME, ContextService.userInfo.getUsername());
            editorUser.putString(EMAIL, ContextService.userInfo.getEmail());
            editorUser.apply();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isAccessTokenUpToDate(Context context) {

        if (ContextService.accessTokenExpiryDate != null) {
            long diff = ContextService.accessTokenExpiryDate.getTime() - new Date().getTime();
            return (diff / (60 * 1000)) > 1; // more than 1 minute left
        }

        SharedPreferences sharedPref = context.getSharedPreferences(TOKENS_STORAGE_NAME, Context.MODE_PRIVATE);
        long accessTokenExpiryDateMillis = sharedPref.getLong(ENCODED_ACCESS_TOKEN_EXPIRY_DATE, 0L);

        if (accessTokenExpiryDateMillis > 0) {
            ContextService.accessTokenExpiryDate = new Date(accessTokenExpiryDateMillis);

            long diff = accessTokenExpiryDateMillis - new Date().getTime();
            return (diff / (60 * 1000)) > 1; // more than 1 minute left
        } else {

            return false;
        }
    }

    public static String getAccessToken(Context context) {

        if (ContextService.accessToken != null) {
            return ContextService.accessToken;
        }

        Crypto crypto = getOrCreateCrypto(context);

        try {
            SharedPreferences sharedPref = context.getSharedPreferences(TOKENS_STORAGE_NAME, Context.MODE_PRIVATE);
            String encodedAccessToken = sharedPref.getString(ENCODED_ACCESS_TOKEN, null);
            if (encodedAccessToken == null) {
                return null;
            }

            byte[] cipherText = Base64.decode(encodedAccessToken, Base64.DEFAULT);
            byte[] decryptedAccessToken = crypto.decrypt(cipherText, accessTokenEntity);

            ContextService.accessToken = new String(decryptedAccessToken, Charset.forName("UTF-8"));
            return ContextService.accessToken;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getRefreshToken(Context context) {

        if (ContextService.refreshToken != null) {
            return ContextService.refreshToken;
        }

        Crypto crypto = getOrCreateCrypto(context);

        try {
            SharedPreferences sharedPref = context.getSharedPreferences(TOKENS_STORAGE_NAME, Context.MODE_PRIVATE);
            String encodedRefreshToken = sharedPref.getString(ENCODED_REFRESH_TOKEN, null);
            if (encodedRefreshToken == null) {
                return null;
            }

            byte[] cipherText = Base64.decode(encodedRefreshToken, Base64.DEFAULT);
            byte[] decryptedRefreshToken = crypto.decrypt(cipherText, refreshTokenEntity);

            ContextService.refreshToken = new String(decryptedRefreshToken, Charset.forName("UTF-8"));
            return ContextService.refreshToken;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void setUserInfo(UserInfoResponse userInfo) {
        ContextService.userInfo = userInfo;
    }

    public static UserInfoResponse getUserInfo(Context context) {

        if (ContextService.userInfo != null) {
            return ContextService.userInfo;
        }

        SharedPreferences sharedPrefUser = context.getSharedPreferences(USER_STORAGE_NAME, Context.MODE_PRIVATE);
        Long userId = sharedPrefUser.getLong(USER_ID, 0L);
        String username = sharedPrefUser.getString(USERNAME, null);
        String email = sharedPrefUser.getString(EMAIL, null);

        UserInfoResponse userInfo = new UserInfoResponse();
        userInfo.setUserId(userId);
        userInfo.setUsername(username);
        userInfo.setEmail(email);

        ContextService.userInfo = userInfo;
        return ContextService.userInfo;
    }

    public static ApplicationMode getApplicationMode() {
        return ContextService.applicationMode;
    }

    public static WebSocket getWebSocket() {
        return webSocket;
    }

    public static void saveWebSocket(WebSocket webSocket) {
        ContextService.webSocket = webSocket;
    }

    public static Long getDriveRequestId() {
        return driveRequestId;
    }

    public static void setDriveRequestId(Long driveRequestId) {
        ContextService.driveRequestId = driveRequestId;
    }

    public static Long getDriveRouteId() {
        return driveRouteId;
    }

    public static void setDriveRouteId(Long driveRouteId) {
        ContextService.driveRouteId = driveRouteId;
    }

    private synchronized static Crypto getOrCreateCrypto(Context context) {

        if (ContextService.crypto == null) {
            // Creates a new Crypto object with default implementations of a key chain
            KeyChain keyChain = new SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256);
            ContextService.crypto = AndroidConceal.get().createDefaultCrypto(keyChain);

            // Check for whether the crypto functionality is available
            // This might fail if Android does not load libaries correctly.
            if (!ContextService.crypto.isAvailable()) {
                ContextService.crypto = null;
            }
        }

        return ContextService.crypto;
    }
}
