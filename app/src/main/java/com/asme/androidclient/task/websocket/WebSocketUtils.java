package com.asme.androidclient.task.websocket;

import com.asme.common.dto.messaging.ConnectMessage;
import com.asme.common.dto.messaging.request.StartRequestMessage;
import com.asme.common.dto.messaging.request.StartRouteMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by vmy on 13.04.18.
 */

public class WebSocketUtils {

    protected static ObjectMapper mapper = new ObjectMapper();

    public static String buildConnectMessage() {
        try {
            return mapper.writeValueAsString(new ConnectMessage());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String buildStartRouteMessage(Long planRouteId) {
        try {
            StartRouteMessage message = new StartRouteMessage();
            message.setPlanRouteId(planRouteId);

            return mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String buildStartRequestMessage(Long planRequestId) {
        try {
            StartRequestMessage message = new StartRequestMessage();
            message.setPlanRequestId(planRequestId);

            return mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
