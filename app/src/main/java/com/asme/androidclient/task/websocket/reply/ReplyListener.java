package com.asme.androidclient.task.websocket.reply;

import com.asme.common.dto.messaging.WebSocketMessage;

/**
 * Created by vmy on 13.04.18.
 */
public interface ReplyListener<T extends WebSocketMessage> {

    void onReply(T reply);
}
