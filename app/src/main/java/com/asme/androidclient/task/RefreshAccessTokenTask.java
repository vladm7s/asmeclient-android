package com.asme.androidclient.task;

import android.content.Context;
import android.util.Log;

import com.asme.androidclient.service.ContextService;
import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.token.RefreshAccessTokenRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.asme.androidclient.service.ContextService.AUTH_SERVER_URL;

/**
 * Created by vmy on 16.04.18.
 */

public class RefreshAccessTokenTask extends BaseAsyncTask<Void, JwtTokenResponse> {

    private static final String URL = AUTH_SERVER_URL + "/token/refresh";

    public RefreshAccessTokenTask(Context context,
                                  Action<JwtTokenResponse> actionOnSuccess,
                                  Action<JwtTokenResponse> actionOnError) {
        super(context, actionOnSuccess, actionOnError);
    }

    @Override
    protected JwtTokenResponse doInBackground(Void... params) {

        String refreshToken = ContextService.getRefreshToken(context);
        if (refreshToken == null) {
            throw new IllegalStateException("Refresh token is missing!");
        }

        RefreshAccessTokenRequest requestDto = new RefreshAccessTokenRequest();
        requestDto.setRefreshTokenContent(refreshToken);

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String json = null;
        try {
            json = mapper.writeValueAsString(requestDto);
        } catch (JsonProcessingException e) {
        }

        Response response = null;
        try {
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            String responseBody = response.body().string();

            System.out.println("\nResponse received:\n" + responseBody + "\n");

            return mapper.readValue(responseBody, JwtTokenResponse.class);
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error refreshing access token: " + e.getMessage(), e);

            JwtTokenResponse tokenResponse = new JwtTokenResponse();
            tokenResponse.setError(true);
            tokenResponse.setMessage(e.getMessage());

            return tokenResponse;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }
}
