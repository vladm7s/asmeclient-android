package com.asme.androidclient.task;

import android.content.Context;
import android.util.Log;

import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.UserLoginRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.asme.androidclient.service.ContextService.AUTH_SERVER_URL;

/**
 * Created by vmy on 05.04.18.
 */
public class UserLoginTask extends BaseAsyncTask<String, JwtTokenResponse> {

    private static final String URL = AUTH_SERVER_URL + "/user/login";

    public UserLoginTask(Context context,
                         Action<JwtTokenResponse> actionOnSuccess,
                         Action<JwtTokenResponse> actionOnError) {
        super(context, actionOnSuccess, actionOnError);
    }

    @Override
    protected JwtTokenResponse doInBackground(String... params) {

        RestTemplate restTemplate = new RestTemplate();

        UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setUsername(params[0]);
        userLoginRequest.setPassword(params[1]);

        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        try {
            ResponseEntity<JwtTokenResponse> response = restTemplate.postForEntity(URL, userLoginRequest, JwtTokenResponse.class);
            return response.getBody();

        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error login: " + e.getMessage(), e);

            JwtTokenResponse tokenResponse = new JwtTokenResponse();
            tokenResponse.setError(true);
            tokenResponse.setMessage(e.getMessage());

            return tokenResponse;
        }
    }

}
