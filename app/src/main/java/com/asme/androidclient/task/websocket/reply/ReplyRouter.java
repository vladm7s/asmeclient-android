package com.asme.androidclient.task.websocket.reply;

import com.asme.common.dto.messaging.MessageType;
import com.asme.common.dto.messaging.WebSocketMessage;
import com.asme.common.dto.messaging.notification.MatchedRequestNotification;
import com.asme.common.dto.messaging.notification.MatchedRouteNotification;
import com.asme.common.dto.messaging.reply.ConnectReply;
import com.asme.common.dto.messaging.reply.StartRequestReply;
import com.asme.common.dto.messaging.reply.StartRouteReply;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vmy on 13.04.18.
 */
public class ReplyRouter {

    private List<ReplyListener<ConnectReply>> connectReplyListeners = new ArrayList<>();

    private List<ReplyListener<StartRequestReply>> startRequestReplyListeners = new ArrayList<>();

    private List<ReplyListener<StartRouteReply>> startRouteReplyListeners = new ArrayList<>();

    private List<ReplyListener<MatchedRequestNotification>> matchedRequestNotificationListeners = new ArrayList<>();

    private List<ReplyListener<MatchedRouteNotification>> matchedRouteNotificationListeners = new ArrayList<>();

    public void route(WebSocketMessage genericReply) {
        MessageType replyType = MessageType.valueOf(genericReply.getType());

        switch (replyType) {
            case CONNECT_REPLY: {
                ConnectReply reply = (ConnectReply) genericReply;

                for (ReplyListener<ConnectReply> listener : connectReplyListeners) {
                    listener.onReply(reply);
                }

                return;
            }
            case START_REQUEST_REPLY: {
                StartRequestReply reply = (StartRequestReply) genericReply;

                for (ReplyListener<StartRequestReply> listener : startRequestReplyListeners) {
                    listener.onReply(reply);
                }

                return;
            }
            case START_ROUTE_REPLY: {
                StartRouteReply reply = (StartRouteReply) genericReply;

                for (ReplyListener<StartRouteReply> listener : startRouteReplyListeners) {
                    listener.onReply(reply);
                }

                return;
            }
            case MATCHED_REQUEST_NOTIFICATION: {
                MatchedRequestNotification reply = (MatchedRequestNotification) genericReply;

                for (ReplyListener<MatchedRequestNotification> listener : matchedRequestNotificationListeners) {
                    listener.onReply(reply);
                }

                return;
            }
            case MATCHED_ROUTE_NOTIFICATION: {
                MatchedRouteNotification reply = (MatchedRouteNotification) genericReply;

                for (ReplyListener<MatchedRouteNotification> listener : matchedRouteNotificationListeners) {
                    listener.onReply(reply);
                }

                return;
            }
            default: {
                throw new IllegalArgumentException("Reply type " + replyType + " not supported");
            }
        }
    }

    public void addConnectReplyListener(ReplyListener<ConnectReply> listener) {
        connectReplyListeners.add(listener);
    }

    public void addStartRequestReplyListener(ReplyListener<StartRequestReply> listener) {
        startRequestReplyListeners.add(listener);
    }

    public void addStartRouteReplyListener(ReplyListener<StartRouteReply> listener) {
        startRouteReplyListeners.add(listener);
    }

    public void addMatchedRequestNotificationListener(ReplyListener<MatchedRequestNotification> listener) {
        matchedRequestNotificationListeners.add(listener);
    }

    public void addMatchedRouteNotificationListener(ReplyListener<MatchedRouteNotification> listener) {
        matchedRouteNotificationListeners.add(listener);
    }
}
