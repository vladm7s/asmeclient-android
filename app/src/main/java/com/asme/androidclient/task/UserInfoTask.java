package com.asme.androidclient.task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.asme.androidclient.service.ContextService;
import com.asme.common.dto.authorization.UserInfoResponse;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.asme.androidclient.service.ContextService.AUTH_SERVER_URL;

/**
 * Created by vmy on 05.04.18.
 */
public class UserInfoTask extends BaseAsyncTask<Void, UserInfoResponse> {

    private static final String URL = AUTH_SERVER_URL + "/user/me";

    public UserInfoTask(Context context) {
        super(context, new Action<UserInfoResponse>() {
            @Override
            public void action(UserInfoResponse result) {

            }
        }, new Action<UserInfoResponse>() {
            @Override
            public void action(UserInfoResponse result) {

            }
        });
    }

    public UserInfoTask(Context context,
                        BaseAsyncTask.Action<UserInfoResponse> actionOnSuccess,
                        BaseAsyncTask.Action<UserInfoResponse> actionOnError) {
        super(context, actionOnSuccess, actionOnError);
    }

    @Override
    protected UserInfoResponse doInBackground(Void... params) {

        String accessToken = ContextService.getAccessToken(context);
        if (accessToken == null) {
            throw new IllegalStateException("Access token is missing!");
        }

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        Response response = null;
        try {
            Request request = new Request.Builder()
                    .url(URL)
                    .header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", "Bearer " + accessToken)
                    .get()
                    .build();

            response = client.newCall(request).execute();
            String responseBody = response.body().string();

            System.out.println("\nResponse received:\n" + responseBody + "\n");

            return mapper.readValue(responseBody, UserInfoResponse.class);
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error retrieving user info: " + e.getMessage(), e);

            UserInfoResponse userInfoResponse = new UserInfoResponse();
            userInfoResponse.setError(true);
            userInfoResponse.setMessage(e.getMessage());

            return userInfoResponse;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    @Override
    protected void onPostExecute(final UserInfoResponse dto) {

        if (dto == null || dto.isError()) {

            String message = "Error retrieving user info" + (dto != null ? ": " + dto.getMessage() : "");
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

            actionOnError.action(dto);
        } else {
            ContextService.setUserInfo(dto);

            actionOnSuccess.action(dto);
        }
    }

}
