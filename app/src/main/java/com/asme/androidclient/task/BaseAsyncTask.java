package com.asme.androidclient.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.asme.common.dto.StatusDto;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by vmy on 06.04.18.
 */
public abstract class BaseAsyncTask<Param, R extends StatusDto> extends AsyncTask<Param, Void, R> {

    protected static ObjectMapper mapper = new ObjectMapper();

    protected final Context context;
    protected final Action<R> actionOnSuccess;
    protected final Action<R> actionOnError;

    protected BaseAsyncTask(Context context, Action<R> actionOnSuccess, Action<R> actionOnError) {
        this.context = context;
        this.actionOnSuccess = actionOnSuccess;
        this.actionOnError = actionOnError;
    }

    @Override
    protected void onPostExecute(R dto) {

        if (dto == null) {
            return;
        }

        if (dto.isError()) {
            String message = "Error in " + this.getClass().getSimpleName() + ": " + dto.getMessage();
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();

            actionOnError.action(dto);
        } else {
            actionOnSuccess.action(dto);
        }
    }

    @Override
    protected void onCancelled() {
        Toast.makeText(context, this.getClass().getSimpleName() + " cancelled", Toast.LENGTH_LONG).show();

        actionOnError.action(null);
    }

    public interface Action<R extends StatusDto> {
        void action(R result);
    }
}
