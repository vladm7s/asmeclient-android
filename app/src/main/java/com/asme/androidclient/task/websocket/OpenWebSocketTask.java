package com.asme.androidclient.task.websocket;

import android.content.Context;

import com.asme.androidclient.domain.WebSocketDto;
import com.asme.androidclient.service.ContextService;
import com.asme.androidclient.task.BaseAsyncTask;
import com.asme.androidclient.task.websocket.reply.ReplyRouter;
import com.asme.common.dto.messaging.WebSocketMessage;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

import static com.asme.androidclient.service.ContextService.WEBSOCKET_API_SERVER_URL;

/**
 * Created by vmy on 12.04.18.
 */

public class OpenWebSocketTask extends BaseAsyncTask<Void, WebSocketDto> {

    private static final String URL = WEBSOCKET_API_SERVER_URL + "/web-socket/api/handler";

    private final ReplyRouter replyRouter;

    public OpenWebSocketTask(Context context,
                             ReplyRouter replyRouter,
                             Action<WebSocketDto> actionOnSuccess,
                             Action<WebSocketDto> actionOnError) {
        super(context, actionOnSuccess, actionOnError);
        this.replyRouter = replyRouter;
    }

    @Override
    protected WebSocketDto doInBackground(Void... voids) {

        String accessToken = ContextService.getAccessToken(context);
        if (accessToken == null) {
            throw new IllegalStateException("Access token is missing!");
        }

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder = requestBuilder.header("Authorization", "Bearer " + accessToken);

        Request request = requestBuilder.url(URL).build();
        client.newWebSocket(request, new WebSocketListener() {

            public void onOpen(WebSocket webSocket, Response response) {
                System.out.println("\nOpened web-socket: " + response.toString() + "\n");

                WebSocketDto webSocketDto = new WebSocketDto();
                webSocketDto.setWebSocket(webSocket);

                actionOnSuccess.action(webSocketDto);
            }

            public void onMessage(WebSocket webSocket, String text) {
                System.out.println("\nMessage received: " + text + "\n");

                try {
                    WebSocketMessage webSocketMessage = mapper.readValue(text, WebSocketMessage.class);
                    replyRouter.route(webSocketMessage);

                } catch (Exception e) {

                    WebSocketDto webSocketDto = new WebSocketDto();
                    webSocketDto.setError(true);
                    webSocketDto.setMessage(e.getMessage());

                    actionOnError.action(webSocketDto);
                }
            }

            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                System.out.println("\nFailure: " + t.getMessage() + "\n");

                WebSocketDto webSocketDto = new WebSocketDto();
                webSocketDto.setWebSocket(webSocket);
                webSocketDto.setError(true);
                webSocketDto.setMessage(t.getMessage());

                actionOnError.action(webSocketDto);
            }

            public void onClosing(WebSocket webSocket, int code, String reason) {
                System.out.println("\nClosing: " + code + ", " + reason + "\n");

                WebSocketDto webSocketDto = new WebSocketDto();
                webSocketDto.setWebSocket(webSocket);
                webSocketDto.setClosing(true);
                webSocketDto.setClosingCode(code);
                webSocketDto.setClosingReason(reason);

                actionOnError.action(webSocketDto);
            }
        });

        return null;
    }
}
