package com.asme.androidclient.task;

import android.content.Context;
import android.util.Log;

import com.asme.common.dto.authorization.JwtTokenResponse;
import com.asme.common.dto.authorization.UserSignUpRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.asme.androidclient.service.ContextService.AUTH_SERVER_URL;

/**
 * Created by vmy on 05.04.18.
 */

public class UserSignUpTask extends BaseAsyncTask<String, JwtTokenResponse> {

    private static final String URL = AUTH_SERVER_URL + "/user/sign-up";

    public UserSignUpTask(Context context,
                          Action<JwtTokenResponse> actionOnSuccess,
                          Action<JwtTokenResponse> actionOnError) {
        super(context, actionOnSuccess, actionOnError);
    }

    @Override
    protected JwtTokenResponse doInBackground(String... params) {

        UserSignUpRequest userSignUpRequest = new UserSignUpRequest();
        userSignUpRequest.setUsername(params[0]);
        userSignUpRequest.setEmail(params[1]);
        userSignUpRequest.setPassword(params[2]);

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String json = null;
        try {
            json = mapper.writeValueAsString(userSignUpRequest);
        } catch (JsonProcessingException e) {
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);

        Response response = null;
        try {
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            String responseBody = response.body().string();

            System.out.println("\nResponse received:\n" + responseBody + "\n");

            return mapper.readValue(responseBody, JwtTokenResponse.class);

        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error sign-up: " + e.getMessage(), e);

            JwtTokenResponse tokenResponse = new JwtTokenResponse();
            tokenResponse.setError(true);
            tokenResponse.setMessage(e.getMessage());

            return tokenResponse;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

}
