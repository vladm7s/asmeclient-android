package com.asme.androidclient.task;

import android.content.Context;
import android.util.Log;

import com.asme.androidclient.service.ContextService;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.common.dto.route.PlanRouteRequest;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.asme.androidclient.service.ContextService.API_SERVER_URL;

/**
 * Created by vmy on 06.04.18.
 */
public class PlanRouteTask extends BaseAsyncTask<PlanRouteRequest, PlanRouteDto> {

    private static final String URL = API_SERVER_URL + "/plan/route/";

    public PlanRouteTask(Context context,
                         Action<PlanRouteDto> actionOnSuccess,
                         Action<PlanRouteDto> actionOnError) {
        super(context, actionOnSuccess, actionOnError);
    }

    @Override
    protected PlanRouteDto doInBackground(PlanRouteRequest... routeRequests) {

        String accessToken = ContextService.getAccessToken(context);
        if (accessToken == null) {
            throw new IllegalStateException("Access token is missing!");
        }

        PlanRouteRequest routeRequest = routeRequests[0];

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30,  TimeUnit.SECONDS)
                .build();

        String json = null;
        try {
            json = mapper.writeValueAsString(routeRequest);
        } catch (JsonProcessingException e) {
        }

        Response response = null;
        try {
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);
            Request request = new Request.Builder()
                    .url(URL)
                    .header("Authorization", "Bearer " + accessToken)
                    .post(body)
                    .build();

            response = client.newCall(request).execute();
            String responseBody = response.body().string();

            System.out.println("\nResponse received:\n" + responseBody + "\n");

            return mapper.readValue(responseBody, PlanRouteDto.class);
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error planning route: " + e.getMessage(), e);

            PlanRouteDto planRouteDto = new PlanRouteDto();
            planRouteDto.setError(true);
            planRouteDto.setMessage(e.getMessage());

            return planRouteDto;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

}
