package com.asme.androidclient;

import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.asme.androidclient.task.BaseAsyncTask;
import com.asme.androidclient.task.PlanRouteTask;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.common.dto.route.PlanRouteRequest;
import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.utils.CommonGeoUtils;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.commons.models.Position;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vmy on 09.10.17.
 */
public class DirectionsActivity extends AppCompatActivity {

    private static final String TAG = "DirectionsActivity";

    private final Position originCamera = Position.fromCoordinates(30.387588, 50.452806);

    private MapView mapView;
    private MapboxMap map;
    private PlanRouteDto currentRoute;
    private Polyline currentRouteOnMap;

    private ImageView hoveringMarker;
    private Button selectLocationButton;
    private FloatingActionButton clearLocationButton;

    private List<Marker> droppedMarkers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Mapbox access token is configured here. This needs to be called either in your application
        // object or in the same activity which contains the mapview.
        Mapbox.getInstance(this, "sk."); //Mapbox.getInstance(this, getString(R.string.access_token));

        // This contains the MapView in XML and needs to be called after the access token is configured.
        //setContentView(R.layout.activity_mas_directions);
        setContentView(R.layout.activity_main);

        // Alhambra landmark in Granada, Spain.

        // Plaza del Triunfo in Granada, Spain.


        // Setup the MapView
        mapView = (MapView) findViewById(R.id.mapView2);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                map = mapboxMap;

                // Once map is ready, we want to position the camera above the user location. We
                // first check that the user has granted the location permission, then we call
                // setInitialCamera.
//                permissionsManager = new PermissionsManager(DirectionsActivity.this);
//                if (!PermissionsManager.areLocationPermissionsGranted(DirectionsActivity.this)) {
//                    permissionsManager.requestLocationPermissions(DirectionsActivity.this);
//                } else {

                setInitialCamera();
                //}

                // Toast instructing user to tap on the map
                Toast.makeText(
                        DirectionsActivity.this,
                        getString(R.string.move_map_instruction),
                        Toast.LENGTH_LONG
                ).show();

                // Add origin and destination to the map
//                mapboxMap.addMarker(new MarkerOptions()
//                        .position(new LatLng(origin.getLatitude(), origin.getLongitude()))
//                        .title(getString(R.string.directions_activity_marker_options_origin_title)));
//                mapboxMap.addMarker(new MarkerOptions()
//                        .position(new LatLng(destination.getLatitude(), destination.getLongitude()))
//                        .title(getString(R.string.directions_activity_marker_options_destination_title)));

                // Get route from API
                //getRoute(origin, destination);
            }
        });

        // When user is still picking a location, we hover a marker above the map in the center.
        // This is done by using an image view with the default marker found in the SDK. You can
        // swap out for your own marker image, just make sure it matches up with the dropped marker.
        hoveringMarker = new ImageView(this);
        hoveringMarker.setImageResource(R.drawable.red_marker);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        hoveringMarker.setLayoutParams(params);
        mapView.addView(hoveringMarker);

        // Button for user to drop marker or to pick marker back up.
        selectLocationButton = (Button) findViewById(R.id.select_location_button);
        selectLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (map != null) {

                    // We first find where the hovering marker position is relative to the map.
                    // Then we set the visibility to gone.
                    float coordinateX = hoveringMarker.getLeft() + (hoveringMarker.getWidth() / 2);
                    float coordinateY = hoveringMarker.getBottom();
                    float[] coords = new float[]{coordinateX, coordinateY};
                    final LatLng latLng = map.getProjection().fromScreenLocation(new PointF(coords[0], coords[1]));
                    //hoveringMarker.setVisibility(View.GONE);

                    // Transform the appearance of the button to become the cancel button
                    //selectLocationButton.setBackgroundColor(
                    //        ContextCompat.getColor(DirectionsActivity.this, R.color.colorAccent));
                    selectLocationButton.setText(getString(R.string.location_picker_select_location_button_select_next));

                    // Create the marker icon the dropped marker will be using.
                    Icon icon = IconFactory.getInstance(DirectionsActivity.this).fromResource(R.drawable.red_marker);

                    // Placing the marker on the map as soon as possible causes the illusion
                    // that the hovering marker and dropped marker are the same.
                    droppedMarkers.add(map.addMarker(new MarkerViewOptions().position(latLng).icon(icon)));

                    // Finally we get the geocoding information
                    //reverseGeocode(latLng);


                    // Switch the button apperance back to select a location.

                    // Lastly, set the hovering marker back to visible.
                    //hoveringMarker.setVisibility(View.VISIBLE);
                    //droppedMarker = null;

                    if (droppedMarkers.size() > 1) {
                        getRoute(droppedMarkers);
                    }
                }
            }
        });

        clearLocationButton = (FloatingActionButton) findViewById(R.id.clearLocationButton);
        clearLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Marker marker : droppedMarkers) {
                    // When the marker is dropped, the user has clicked the button to cancel.
                    // Therefore, we pick the marker back up.
                    map.removeMarker(marker);
                }
                droppedMarkers.clear();

                if (currentRouteOnMap != null) {
                    map.removePolyline(currentRouteOnMap);
                    currentRouteOnMap = null;
                }

                selectLocationButton.setBackgroundColor(
                        ContextCompat.getColor(DirectionsActivity.this, R.color.colorPrimary));
                selectLocationButton.setText(getString(R.string.location_picker_select_location_button_select_start));
            }
        });
    }

    private void setInitialCamera() {
        // Method is used to set the initial map camera position. Should only be called once when
        // the map is ready. We first try using the users last location so we can quickly set the
        // camera as fast as possible.
        //if (locationEngine.getLastLocation() != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originCamera.getLatitude(), originCamera.getLongitude()), 14));
        //}

        // This location listener is used in a very specific use case. If the users last location is
        // unknown we wait till the GPS locates them and position the camera above.
//        locationEngineListener = new LocationEngineListener() {
//            @Override
//            public void onConnected() {
//                locationEngine.requestLocationUpdates();
//            }
//
//            @Override
//            public void onLocationChanged(Location location) {
//                if (location != null) {
//                    // Move the map camera to where the user location is
//                    map.setCameraPosition(new CameraPosition.Builder()
//                            .target(new LatLng(location))
//                            .zoom(16)
//                            .build());
//                    locationEngine.removeLocationEngineListener(this);
//                }
//            }
//        };
//        locationEngine.addLocationEngineListener(locationEngineListener);
//        // Enable the location layer on the map and track the user location until they perform a
//        // map gesture.
//        map.setMyLocationEnabled(true);
//        map.getTrackingSettings().setMyLocationTrackingMode(MyLocationTracking.TRACKING_FOLLOW);
    } // End setInitialCamera

    private void getRoute(List<Marker> markers) {

        PlanRouteRequest routeRequest = new PlanRouteRequest();
        for (Marker marker : markers) {
            routeRequest.addWaypoint(new GeoPoint(
                    String.valueOf(marker.getPosition().getLongitude()),
                    String.valueOf(marker.getPosition().getLatitude())
            ));
        }

        PlanRouteTask planRouteTask = new PlanRouteTask(this,
                new BaseAsyncTask.Action<PlanRouteDto>() {
                    @Override
                    public void action(PlanRouteDto result) {

                        currentRoute = result;
                        drawRoute(currentRoute);

                        Log.d(TAG, "Distance: " + currentRoute.getPlanDistance());
                        Toast.makeText(DirectionsActivity.this, String.format(getString(R.string.directions_activity_toast_message),
                                currentRoute.getPlanDistance()), Toast.LENGTH_SHORT).show();
                    }
                },
                new BaseAsyncTask.Action<PlanRouteDto>() {
                    @Override
                    public void action(PlanRouteDto result) {

                    }
                });

        planRouteTask.execute(routeRequest);
    }

    private void drawRoute(PlanRouteDto route) {

        if (currentRouteOnMap != null) {
            map.removePolyline(currentRouteOnMap);
            currentRouteOnMap = null;
        }

        GeoShapeLine polyline = CommonGeoUtils.fromGeometry(route.getPlanGeometry(), 5);
        List<BigDecimal[]> coordinates = polyline.getCoordinates();
        LatLng[] points = new LatLng[coordinates.size()];

        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(coordinates.get(i)[1].doubleValue(), coordinates.get(i)[0].doubleValue());
        }

        // Draw Points on MapView
        currentRouteOnMap = map.addPolyline(new PolylineOptions()
                .add(points)
                .color(Color.parseColor("#009688"))
                .width(5));
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Cancel the directions API request
        //if (client != null) {
        //    client.cancelCall();
        //}
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
