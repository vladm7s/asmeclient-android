package com.asme.androidclient.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.asme.androidclient.MainApplicationActivity;
import com.asme.androidclient.R;
import com.asme.androidclient.service.ContextService;
import com.asme.androidclient.task.BaseAsyncTask;
import com.asme.androidclient.task.UserLoginTask;
import com.asme.common.dto.authorization.JwtTokenResponse;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask loginTask = null;

    // UI references.
    private EditText usernameView;
    private EditText passwordView;
    private View progressView;
    private View loginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        usernameView = (EditText) findViewById(R.id.login_username);
        passwordView = (EditText) findViewById(R.id.login_password);

        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        TextView registerNow = (TextView) findViewById(R.id.register_now);
        registerNow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (loginTask != null) {
            return;
        }

        // Reset errors
        usernameView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the login attempt.
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        if (isUsernameValid(username) && isPasswordValid(password)) {

            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            loginTask = new UserLoginTask(this,
                    new BaseAsyncTask.Action<JwtTokenResponse>() {
                        @Override
                        public void action(JwtTokenResponse tokenResponse) {
                            loginTask = null;
                            showProgress(false);

                            ContextService.saveToken(LoginActivity.this, tokenResponse);

                            Intent intent = new Intent(LoginActivity.this, MainApplicationActivity.class);
                            startActivity(intent);

                            LoginActivity.this.finish();
                        }
                    },
                    new BaseAsyncTask.Action<JwtTokenResponse>() {
                        @Override
                        public void action(JwtTokenResponse tokenResponse) {
                            loginTask = null;
                            showProgress(false);

                            if (tokenResponse != null) {
                                passwordView.setError(tokenResponse.getMessage());
                                passwordView.requestFocus();
                            }
                        }
                    });

            loginTask.execute(username, password);
        }
    }

    private boolean isUsernameValid(String username) {

        // Check for a valid username
        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.error_field_required));
            usernameView.requestFocus();

            return false;
        } else
        if (!isUsernameFormatValid(username)) {
            usernameView.setError(getString(R.string.error_invalid_username));
            usernameView.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isPasswordValid(String password) {

        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            passwordView.requestFocus();

            return false;
        } else
        if (!isPasswordFormatValid(password)) {
            passwordView.setError(getString(R.string.error_invalid_password));
            passwordView.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isUsernameFormatValid(String username) {
        return username.length() > 2;
    }

    private boolean isPasswordFormatValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

