package com.asme.androidclient.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.asme.androidclient.MainApplicationActivity;
import com.asme.androidclient.R;
import com.asme.androidclient.service.ContextService;
import com.asme.androidclient.task.BaseAsyncTask;
import com.asme.androidclient.task.UserSignUpTask;
import com.asme.common.dto.authorization.JwtTokenResponse;

/**
 * A login screen that offers login via email/password.
 */
public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpActivity";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserSignUpTask signUpTask = null;

    // UI references
    private EditText usernameView;
    private AutoCompleteTextView emailView;
    private EditText passwordView;
    private View progressView;
    private View formView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setupActionBar();

        usernameView = (EditText) findViewById(R.id.register_username);
        emailView = (AutoCompleteTextView) findViewById(R.id.register_email);
        passwordView = (EditText) findViewById(R.id.register_password);

        Button signUpButton = (Button) findViewById(R.id.register_button);
        signUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignUp();
            }
        });

        formView = findViewById(R.id.register_form);
        progressView = findViewById(R.id.register_progress);
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptSignUp() {

        if (signUpTask != null) {
            return;
        }

        // Reset errors
        usernameView.setError(null);
        emailView.setError(null);
        passwordView.setError(null);

        // Store values at the time of the signUp attempt
        String username = usernameView.getText().toString();
        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();

        if (isUsernameValid(username)
                && isEmailValid(email)
                && isPasswordValid(password)) {

            // Show a progress spinner, and kick off a background task to
            // perform the user signUp attempt
            showProgress(true);

            signUpTask = new UserSignUpTask(this,
                    new BaseAsyncTask.Action<JwtTokenResponse>() {
                        @Override
                        public void action(JwtTokenResponse tokenResponse) {
                            signUpTask = null;
                            showProgress(false);

                            ContextService.saveToken(SignUpActivity.this, tokenResponse);

                            Intent intent = new Intent(SignUpActivity.this, MainApplicationActivity.class);
                            startActivity(intent);

                            SignUpActivity.this.finish();
                        }
                    },
                    new BaseAsyncTask.Action<JwtTokenResponse>() {
                        @Override
                        public void action(JwtTokenResponse tokenResponse) {
                            signUpTask = null;
                            showProgress(false);

                            if (tokenResponse != null) {
                                passwordView.setError(tokenResponse.getMessage());
                                passwordView.requestFocus();
                            }
                        }
                    });

            signUpTask.execute(username, email, password);
        }
    }

    private boolean isUsernameValid(String username) {

        // Check for a valid username
        if (TextUtils.isEmpty(username)) {
            usernameView.setError(getString(R.string.error_field_required));
            usernameView.requestFocus();

            return false;
        } else
        if (!isUsernameFormatValid(username)) {
            usernameView.setError(getString(R.string.error_invalid_username));
            usernameView.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isPasswordValid(String password) {

        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            passwordView.setError(getString(R.string.error_field_required));
            passwordView.requestFocus();

            return false;
        } else
        if (!isPasswordFormatValid(password)) {
            passwordView.setError(getString(R.string.error_invalid_password));
            passwordView.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isEmailValid(String email) {

        // Check for a valid email address
        if (TextUtils.isEmpty(email)) {
            emailView.setError(getString(R.string.error_field_required));
            emailView.requestFocus();

            return false;
        } else
        if (!isEmailFormatValid(email)) {
            emailView.setError(getString(R.string.error_invalid_email));
            emailView.requestFocus();

            return false;
        }

        return true;
    }

    private boolean isUsernameFormatValid(String username) {
        return username.length() > 2;
    }

    private boolean isEmailFormatValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordFormatValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

