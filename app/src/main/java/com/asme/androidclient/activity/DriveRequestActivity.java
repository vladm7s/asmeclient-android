package com.asme.androidclient.activity;

import android.os.Bundle;

import com.asme.androidclient.R;
import com.mapbox.mapboxsdk.Mapbox;

public class DriveRequestActivity extends BaseMapboxActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, "sk.");

        setContentView(R.layout.activity_drive_request);

        initMapbox(savedInstanceState);
    }
}
