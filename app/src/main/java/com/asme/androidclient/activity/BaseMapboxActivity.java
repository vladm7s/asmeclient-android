package com.asme.androidclient.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.asme.androidclient.R;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.commons.models.Position;

/**
 * Created by vmy on 17.04.18.
 */
public abstract class BaseMapboxActivity extends AppCompatActivity {

    protected MapView mapView;
    protected MapboxMap map;
    protected Position originCamera = Position.fromCoordinates(30.387588, 50.452806);

    protected void initMapbox(Bundle savedInstanceState) {
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                map = mapboxMap;

                setInitialCamera();
            }
        });
    }

    protected void setInitialCamera() {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originCamera.getLatitude(), originCamera.getLongitude()), 14));
    }
}
