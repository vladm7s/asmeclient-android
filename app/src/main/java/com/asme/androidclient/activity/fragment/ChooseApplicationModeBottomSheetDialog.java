package com.asme.androidclient.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asme.androidclient.R;
import com.asme.androidclient.domain.ApplicationMode;

/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     ChooseApplicationModeBottomSheetDialog.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {@link ChooseApplicationModeBottomSheetDialog.Listener}.</p>
 */
public class ChooseApplicationModeBottomSheetDialog extends BottomSheetDialogFragment {

    private Listener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chooseapplicationmode_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View driveRouteMode = view.findViewById(R.id.application_mode_drive_route);
        driveRouteMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChooseApplicationModeClicked(ApplicationMode.DRIVE);

                dismiss();
            }
        });

        View driveRequestMode = view.findViewById(R.id.application_mode_drive_request);
        driveRequestMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onChooseApplicationModeClicked(ApplicationMode.REQUEST);

                dismiss();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            mListener = (Listener) parent;
        } else {
            mListener = (Listener) context;
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public interface Listener {
        void onChooseApplicationModeClicked(ApplicationMode applicationMode);
    }
}
