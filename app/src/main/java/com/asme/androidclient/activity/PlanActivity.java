package com.asme.androidclient.activity;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.asme.androidclient.R;
import com.asme.androidclient.activity.fragment.ChooseApplicationModeBottomSheetDialog;
import com.asme.androidclient.domain.ApplicationMode;
import com.asme.androidclient.service.ContextService;

import java.util.ArrayList;
import java.util.List;

public class PlanActivity extends AppCompatActivity implements ChooseApplicationModeBottomSheetDialog.Listener {

    private ViewGroup plannerLayout;

    private FloatingActionButton startPlannerButton;

    private MenuItem startConfigurationItem;

    private MenuItem finishConfigurationItem;

    private MenuItem addPointItem;

    private List<View> intermediatePoints = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.plan_activity_toolbar);
        setSupportActionBar(toolbar);

        plannerLayout = (ViewGroup) findViewById(R.id.plannerLayout);

        // start location
        final EditText startLocationText = (EditText) plannerLayout.findViewWithTag("plan_startLocation");
        ImageButton startLocationClearButton = (ImageButton) plannerLayout.findViewWithTag("plan_startLocation_clear_button");
        startLocationClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationText.getText().clear();
            }
        });

        // destination
        final EditText destinationText = (EditText) plannerLayout.findViewWithTag("plan_destination");
        ImageButton destinationClearButton = (ImageButton) plannerLayout.findViewWithTag("plan_destination_clear_button");
        destinationClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destinationText.getText().clear();
            }
        });

        startPlannerButton = (FloatingActionButton) findViewById(R.id.start_planner_button);
        startPlannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Initializing a bottom sheet
                BottomSheetDialogFragment bottomSheetDialogFragment = new ChooseApplicationModeBottomSheetDialog();

                //show it
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });
    }

    @Override
    public void onChooseApplicationModeClicked(ApplicationMode applicationMode) {

        if (applicationMode == ApplicationMode.DRIVE) {
            ContextService.saveApplicationMode(ApplicationMode.DRIVE);

            startPlannerButton.setVisibility(View.GONE);
            plannerLayout.setVisibility(View.VISIBLE);

            startConfigurationItem.expandActionView();
            finishConfigurationItem.setVisible(true);
            addPointItem.setVisible(true);

        } else
        if (applicationMode == ApplicationMode.REQUEST) {
            ContextService.saveApplicationMode(ApplicationMode.REQUEST);

            startPlannerButton.setVisibility(View.GONE);
            plannerLayout.setVisibility(View.VISIBLE);

            startConfigurationItem.expandActionView();
            finishConfigurationItem.setVisible(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_plan, menu);

        // Define the listener
        MenuItemCompat.OnActionExpandListener configurationCollapsedListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when action item collapses

                startPlannerButton.setVisibility(View.VISIBLE);
                plannerLayout.setVisibility(View.GONE);

                finishConfigurationItem.setVisible(false);
                addPointItem.setVisible(false);

                for (View routePoint : intermediatePoints) {
                    plannerLayout.removeView(routePoint);
                }

                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        };

        startConfigurationItem = menu.findItem(R.id.start_configuration);
        MenuItemCompat.setOnActionExpandListener(startConfigurationItem, configurationCollapsedListener);

        finishConfigurationItem = menu.findItem(R.id.finish_configuration);
        addPointItem = menu.findItem(R.id.add_point);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.add_point) {

            final View routePoint = View.inflate(PlanActivity.this, R.layout.route_point, null);
            plannerLayout.addView(routePoint);
            intermediatePoints.add(routePoint);

            EditText editText = (EditText) routePoint.findViewWithTag("route_point_edit_text");
            editText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            ImageButton button = (ImageButton) routePoint.findViewWithTag("route_point_delete_button");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    plannerLayout.removeView(routePoint);
                    intermediatePoints.remove(routePoint);
                }
            });

            return true;
        } else
        if (id == R.id.finish_configuration) {
            startConfigurationItem.collapseActionView();
        }

        return super.onOptionsItemSelected(item);
    }
}
