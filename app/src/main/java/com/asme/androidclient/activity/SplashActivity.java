package com.asme.androidclient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.asme.androidclient.MainApplicationActivity;
import com.asme.androidclient.service.ContextService;
import com.asme.androidclient.task.BaseAsyncTask;
import com.asme.androidclient.task.RefreshAccessTokenTask;
import com.asme.common.dto.authorization.JwtTokenResponse;

/**
 * Created by vmy on 17.04.18.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextService.isAccessTokenUpToDate(SplashActivity.this)) {

            Intent intent = new Intent(SplashActivity.this, MainApplicationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            finish();
        } else
        if (ContextService.getRefreshToken(SplashActivity.this) != null) {

            RefreshAccessTokenTask refreshAccessTokenTask = new RefreshAccessTokenTask(
                    SplashActivity.this,
                    new BaseAsyncTask.Action<JwtTokenResponse>() {
                        @Override
                        public void action(JwtTokenResponse tokenResponse) {

                            ContextService.saveToken(SplashActivity.this, tokenResponse);

                            Intent intent = new Intent(SplashActivity.this, MainApplicationActivity.class);
                            startActivity(intent);

                            SplashActivity.this.finish();
                        }
                    },
                    new BaseAsyncTask.Action<JwtTokenResponse>() {
                        @Override
                        public void action(JwtTokenResponse tokenResponse) {
                            // TODO: pass error message to LoginActivity to display
                            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(intent);

                            SplashActivity.this.finish();
                        }
                    });

            refreshAccessTokenTask.execute();

        } else {
            Intent intent = new Intent(SplashActivity.this, PlanActivity.class);
            startActivity(intent);

            finish();
        }
    }
}
