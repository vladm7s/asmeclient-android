package com.asme.androidclient;

import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.asme.androidclient.domain.ApplicationMode;
import com.asme.androidclient.domain.WebSocketDto;
import com.asme.androidclient.listener.ApplicationModeListener;
import com.asme.androidclient.service.ContextService;
import com.asme.androidclient.task.BaseAsyncTask;
import com.asme.androidclient.task.PlanRequestTask;
import com.asme.androidclient.task.PlanRouteTask;
import com.asme.androidclient.task.websocket.OpenWebSocketTask;
import com.asme.androidclient.task.websocket.WebSocketUtils;
import com.asme.androidclient.task.websocket.reply.ReplyListener;
import com.asme.androidclient.task.websocket.reply.ReplyRouter;
import com.asme.common.dto.messaging.notification.MatchedRequestNotification;
import com.asme.common.dto.messaging.notification.MatchedRouteNotification;
import com.asme.common.dto.messaging.reply.StartRequestReply;
import com.asme.common.dto.messaging.reply.StartRouteReply;
import com.asme.common.dto.request.DriveRequestDto;
import com.asme.common.dto.request.PlanRequestDto;
import com.asme.common.dto.route.DriveRouteDto;
import com.asme.common.dto.route.PlanRouteDto;
import com.asme.common.dto.route.PlanRouteRequest;
import com.asme.common.geo.GeoPoint;
import com.asme.common.geo.GeoShapeLine;
import com.asme.common.utils.CommonGeoUtils;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.services.commons.models.Position;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import okhttp3.WebSocket;

public class MainApplicationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Position originCamera = Position.fromCoordinates(30.387588, 50.452806);

    private MapView mapView;
    private MapboxMap map;

    private Icon icon;
    private ImageView hoveringMarker;

    private Button startButton;
    private Button selectLocationButton;
    private FloatingActionButton clearLocationButton;

    private PlanRequestDto plannedRequest;
    private PlanRouteDto plannedRoute;

    private Polyline currentRouteOnMap;

    private List<Polyline> matchingRoutes = new ArrayList<>();
    private List<Pair<Marker, Marker>> matchingRequests = new ArrayList<>();

    private List<Marker> droppedMarkers = new ArrayList<>();

    private Marker startLocation;
    private Marker destination;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, "sk.");

        setContentView(R.layout.activity_main_application);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        addListenerOnApplicationModeSelection();
        setUsernameAndEmail();

        initMapbox(savedInstanceState);
    }

    private void initMapbox(Bundle savedInstanceState) {

        // Setup the MapView
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                map = mapboxMap;

                setInitialCamera();

                // Toast instructing user to tap on the map
                Toast.makeText(
                        MainApplicationActivity.this,
                        getString(R.string.move_map_instruction),
                        Toast.LENGTH_LONG
                ).show();
            }
        });

        // When user is still picking a location, we hover a marker above the map in the center.
        // This is done by using an image view with the default marker found in the SDK. You can
        // swap out for your own marker image, just make sure it matches up with the dropped marker.
        hoveringMarker = new ImageView(this);
        hoveringMarker.setImageResource(R.drawable.red_marker);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        hoveringMarker.setLayoutParams(params);
        mapView.addView(hoveringMarker);

        icon = IconFactory.getInstance(MainApplicationActivity.this).fromResource(R.drawable.red_marker);

        // Button for user to drop marker or to pick marker back up.
        selectLocationButton = (Button) findViewById(R.id.select_location_button);
        selectLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (map != null) {

                    // We first find where the hovering marker position is relative to the map.
                    // Then we set the visibility to gone.
                    float coordinateX = hoveringMarker.getLeft() + (hoveringMarker.getWidth() / 2);
                    float coordinateY = hoveringMarker.getBottom();
                    float[] coords = new float[]{coordinateX, coordinateY};
                    final LatLng latLng = map.getProjection().fromScreenLocation(new PointF(coords[0], coords[1]));

                    if (ContextService.getApplicationMode() == ApplicationMode.DRIVE) {
                        selectLocationButton.setText(getString(R.string.location_picker_select_location_button_select_next));
                    } else
                    if (ContextService.getApplicationMode() == ApplicationMode.REQUEST) {
                        selectLocationButton.setText(getString(R.string.location_picker_select_location_button_select_destination));
                    }

                    // Create the marker icon the dropped marker will be using.

                    if (ContextService.getApplicationMode() == ApplicationMode.DRIVE) {

                        // Placing the marker on the map as soon as possible causes the illusion
                        // that the hovering marker and dropped marker are the same.
                        droppedMarkers.add(map.addMarker(new MarkerViewOptions().position(latLng).icon(icon)));

                        if (droppedMarkers.size() > 1) {
                            planRoute(droppedMarkers);
                        }
                    } else
                    if (ContextService.getApplicationMode() == ApplicationMode.REQUEST) {

                        if (startLocation == null) {
                            startLocation = map.addMarker(new MarkerViewOptions().position(latLng).icon(icon));
                        } else {
                            destination = map.addMarker(new MarkerViewOptions().position(latLng).icon(icon));
                        }

                        if (startLocation != null && destination != null) {
                            planRequest(startLocation, destination);
                        }
                    }
                }
            }
        });

        final ReplyRouter replyRouter = new ReplyRouter();

        replyRouter.addStartRequestReplyListener(new ReplyListener<StartRequestReply>() {
            @Override
            public void onReply(StartRequestReply reply) {

                ContextService.setDriveRequestId(reply.getDriveRequestId());

                Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                        getString(R.string.started_request),
                        Snackbar.LENGTH_LONG).show();
            }
        });

        replyRouter.addStartRouteReplyListener(new ReplyListener<StartRouteReply>() {
            @Override
            public void onReply(StartRouteReply reply) {

                ContextService.setDriveRouteId(reply.getDriveRouteId());

                Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                        getString(R.string.started_route),
                        Snackbar.LENGTH_LONG).show();
            }
        });

        replyRouter.addMatchedRouteNotificationListener(new ReplyListener<MatchedRouteNotification>() {
            @Override
            public void onReply(MatchedRouteNotification reply) {
                addMatchingRoute(reply.getGeometry());

                Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                        getString(R.string.new_matching_route),
                        Snackbar.LENGTH_LONG).show();
            }
        });

        replyRouter.addMatchedRequestNotificationListener(new ReplyListener<MatchedRequestNotification>() {
            @Override
            public void onReply(MatchedRequestNotification reply) {
                addMatchingRequest(reply.getStartLocation(), reply.getDestination(), icon);

                Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                        getString(R.string.new_matching_request),
                        Snackbar.LENGTH_LONG).show();
            }
        });

        startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OpenWebSocketTask openWebSocketTask = new OpenWebSocketTask(MainApplicationActivity.this, replyRouter,
                        new BaseAsyncTask.Action<WebSocketDto>() {
                            @Override
                            public void action(WebSocketDto result) {
                                WebSocket webSocket = result.getWebSocket();
                                ContextService.saveWebSocket(webSocket);

                                webSocket.send(WebSocketUtils.buildConnectMessage());

                                if (ContextService.getApplicationMode() == ApplicationMode.DRIVE) {
                                    webSocket.send(WebSocketUtils.buildStartRouteMessage(Long.parseLong(plannedRoute.getId())));
                                }

                                if (ContextService.getApplicationMode() == ApplicationMode.REQUEST) {
                                    webSocket.send(WebSocketUtils.buildStartRequestMessage(Long.parseLong(plannedRequest.getId())));
                                }
                            }
                        },
                        new BaseAsyncTask.Action<WebSocketDto>() {
                            @Override
                            public void action(WebSocketDto result) {

                                if (result.isClosing()) {
                                    Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                                            String.format(getString(R.string.connection_is_closed), result.getClosingReason()),
                                            Snackbar.LENGTH_LONG).show();

                                    ContextService.saveWebSocket(null);
                                } else {
                                    Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                                            String.format(getString(R.string.error_connecting), result.getMessage()),
                                            Snackbar.LENGTH_LONG).show();
                                }
                            }
                        });

                openWebSocketTask.execute();
            }
        });

        clearLocationButton = (FloatingActionButton) findViewById(R.id.clearLocationButton);
        clearLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (startLocation != null) {
                    map.removeMarker(startLocation);
                    startLocation = null;
                }

                if (destination != null) {
                    map.removeMarker(destination);
                    destination = null;
                }

                for (Marker marker : droppedMarkers) {
                    map.removeMarker(marker);
                }
                droppedMarkers.clear();

                if (currentRouteOnMap != null) {
                    map.removePolyline(currentRouteOnMap);
                    currentRouteOnMap = null;
                }

                selectLocationButton.setBackgroundColor(
                        ContextCompat.getColor(MainApplicationActivity.this, R.color.colorPrimary));
                selectLocationButton.setText(getString(R.string.location_picker_select_location_button_select_start));
            }
        });
    }

    public void setUsernameAndEmail() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        LinearLayout headerLayout = (LinearLayout) navigationView.getHeaderView(0);

        TextView username = (TextView) headerLayout.findViewById(R.id.username);
        TextView email = (TextView) headerLayout.findViewById(R.id.email);

        username.setText(ContextService.getUserInfo(MainApplicationActivity.this).getUsername());
        email.setText(ContextService.getUserInfo(MainApplicationActivity.this).getEmail());
    }

    public void addListenerOnApplicationModeSelection() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        LinearLayout headerLayout = (LinearLayout) navigationView.getHeaderView(0);
        Spinner spinnerAppMode = (Spinner) headerLayout.findViewById(R.id.spinner_app_mode);

        spinnerAppMode.setOnItemSelectedListener(new ApplicationModeListener());

        List<ApplicationMode> modes = new ArrayList<>();
        modes.add(ApplicationMode.DRIVE);
        modes.add(ApplicationMode.REQUEST);

        ArrayAdapter<ApplicationMode> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, modes);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerAppMode.setAdapter(dataAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_application, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setInitialCamera() {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originCamera.getLatitude(), originCamera.getLongitude()), 14));
    }

    private void planRequest(Marker start, Marker destination) {

        final PlanRequestDto planRequest = new PlanRequestDto();
        planRequest.setStartLocation(new GeoPoint(
                String.valueOf(start.getPosition().getLongitude()),
                String.valueOf(start.getPosition().getLatitude())
        ));
        planRequest.setDestination(new GeoPoint(
                String.valueOf(destination.getPosition().getLongitude()),
                String.valueOf(destination.getPosition().getLatitude())
        ));
        planRequest.setStartLocationRadius(50);
        planRequest.setDestinationRadius(50);

        PlanRequestTask planRequestTask = new PlanRequestTask(this,
                new BaseAsyncTask.Action<PlanRequestDto>() {
                    @Override
                    public void action(PlanRequestDto result) {

                        plannedRequest = result;
                        drawMatchingRoutes(plannedRequest);

                        Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                                String.format(getString(R.string.found_matched_routes), plannedRequest.getDriveRoutes().size()),
                                Snackbar.LENGTH_LONG).show();
                    }
                },
                new BaseAsyncTask.Action<PlanRequestDto>() {
                    @Override
                    public void action(PlanRequestDto result) {

                    }
                });

        planRequestTask.execute(planRequest);
    }

    private void planRoute(List<Marker> markers) {

        PlanRouteRequest planRoute = new PlanRouteRequest();
        for (Marker marker : markers) {
            planRoute.addWaypoint(new GeoPoint(
                    String.valueOf(marker.getPosition().getLongitude()),
                    String.valueOf(marker.getPosition().getLatitude())
            ));
        }

        PlanRouteTask planRouteTask = new PlanRouteTask(this,
                new BaseAsyncTask.Action<PlanRouteDto>() {
                    @Override
                    public void action(PlanRouteDto result) {

                        plannedRoute = result;
                        drawRoute(plannedRoute);
                        drawMatchingRequests(plannedRoute);

                        Log.d(this.getClass().getSimpleName(), "Distance: " + plannedRoute.getPlanDistance());
                        Toast.makeText(MainApplicationActivity.this, String.format(getString(R.string.directions_activity_toast_message),
                                plannedRoute.getPlanDistance()), Toast.LENGTH_SHORT).show();

                        Snackbar.make(findViewById(R.id.appBarCoordinatorLayout),
                                String.format(getString(R.string.found_matched_requests), plannedRoute.getDriveRequests().size()),
                                Snackbar.LENGTH_LONG).show();
                    }
                },
                new BaseAsyncTask.Action<PlanRouteDto>() {
                    @Override
                    public void action(PlanRouteDto result) {

                    }
                });

        planRouteTask.execute(planRoute);
    }

    private void drawRoute(PlanRouteDto plannedRoute) {

        if (currentRouteOnMap != null) {
            map.removePolyline(currentRouteOnMap);
            currentRouteOnMap = null;
        }

        currentRouteOnMap = drawGeometry(plannedRoute.getPlanGeometry());
    }

    private void drawMatchingRoutes(PlanRequestDto plannedRequest) {

        for (Polyline matchingRoute : matchingRoutes) {
            map.removePolyline(matchingRoute);
        }
        matchingRoutes.clear();

        for (DriveRouteDto driveRouteDto : plannedRequest.getDriveRoutes()) {
            addMatchingRoute(driveRouteDto.getGeometry());
        }
    }

    private void drawMatchingRequests(PlanRouteDto plannedRoute) {

        for (Pair<Marker, Marker> matchingRequest : matchingRequests) {
            map.removeMarker(matchingRequest.first);
            map.removeMarker(matchingRequest.second);
        }
        matchingRequests.clear();

        for (DriveRequestDto driveRequestDto : plannedRoute.getDriveRequests()) {
            addMatchingRequest(driveRequestDto.getStartLocation(), driveRequestDto.getDestination(), icon);
        }
    }

    private void addMatchingRoute(String geometry) {
        matchingRoutes.add(drawGeometry(geometry));
    }

    private void addMatchingRequest(GeoPoint start, GeoPoint destination, Icon icon) {
        matchingRequests.add(drawMarkers(start, destination, icon));
    }

    private Polyline drawGeometry(String geometry) {

        GeoShapeLine polyline = CommonGeoUtils.fromGeometry(geometry, 5);
        List<BigDecimal[]> coordinates = polyline.getCoordinates();
        LatLng[] points = new LatLng[coordinates.size()];

        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(coordinates.get(i)[1].doubleValue(), coordinates.get(i)[0].doubleValue());
        }

        // Draw Points on MapView
        return map.addPolyline(new PolylineOptions()
                .add(points)
                .color(Color.parseColor("#009688"))
                .width(5));
    }

    private Pair<Marker, Marker> drawMarkers(GeoPoint start, GeoPoint destination, Icon icon) {

        LatLng startLatLon = new LatLng(start.getLat().doubleValue(), start.getLon().doubleValue());
        Marker markerStart = map.addMarker(new MarkerViewOptions().position(startLatLon).icon(icon));

        LatLng destinationLatLon = new LatLng(destination.getLat().doubleValue(), destination.getLon().doubleValue());
        Marker markerDestination = map.addMarker(new MarkerViewOptions().position(destinationLatLon).icon(icon));

        return new Pair<>(markerStart, markerDestination);
    }
}
