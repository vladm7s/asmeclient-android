package com.asme.androidclient.domain;

import com.asme.common.dto.StatusDto;

import okhttp3.WebSocket;

/**
 * Created by vmy on 12.04.18.
 */

public class WebSocketDto extends StatusDto {

    private WebSocket webSocket;

    private boolean closing;

    private int closingCode;

    private String closingReason;

    public WebSocket getWebSocket() {
        return webSocket;
    }

    public void setWebSocket(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    public boolean isClosing() {
        return closing;
    }

    public void setClosing(boolean closing) {
        this.closing = closing;
    }

    public int getClosingCode() {
        return closingCode;
    }

    public void setClosingCode(int closingCode) {
        this.closingCode = closingCode;
    }

    public String getClosingReason() {
        return closingReason;
    }

    public void setClosingReason(String closingReason) {
        this.closingReason = closingReason;
    }
}
