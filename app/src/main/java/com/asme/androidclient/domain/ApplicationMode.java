package com.asme.androidclient.domain;

/**
 * Created by vmy on 05.04.18.
 */

public enum ApplicationMode {

    DRIVE(1, "Хочу подвезти"),
    REQUEST(2, "Прошу меня подвезти");

    private long id;
    private String name;

    ApplicationMode(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
