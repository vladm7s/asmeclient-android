package com.asme.androidclient.listener;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.asme.androidclient.domain.ApplicationMode;
import com.asme.androidclient.service.ContextService;

/**
 * Created by vmy on 05.04.18.
 */
public class ApplicationModeListener implements AdapterView.OnItemSelectedListener {

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // On selecting a spinner item
        ApplicationMode applicationMode = (ApplicationMode) parent.getItemAtPosition(position);

        ContextService.saveApplicationMode(applicationMode);

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + applicationMode.getName(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
